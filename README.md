# Custom_Actions.example

This is an example of a project that stores your GitLab (or GitHub) actions. As they may trigger custom runners, the docs recommend to keep the actions in a private repository.

## Branches matter!

Note that in appsettings.json of the main project, you specify branches. These allow to trigger different actions but still have only one actions-project per developped project.

